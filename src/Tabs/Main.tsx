import React from "react"
import Home from './Home.tsx'
import About from "./About.tsx"

const Main = ()=>{
    return(
        <div>
            <Home />

            <About />
        </div>
    )
}

export default Main